var express = require('express');
var app = express();
var fs = require("fs");

app.get('/', function (req, res) {
	var obj = {"pro_plan":"pro_realtime","is_authenticated":true};
   res.json( obj );
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})